" Late file type detection
"
" Maintainer:	Kevin Locke <kevin@kevinlocke.name>
" Last Change:	2021 July 16
"
" File type detection is split into 4 phases (see :help 43.2):
" 1. filetype.vim precise path pattern matching.
" 2. scripts.vim content matching (typically from first few lines).
" 3. filetype.vim loose path pattern matching.
" 4. ftdetect/*.vim from plugins.
"
" This script contains expensive or loose file type checks, which should only
" run if none of the above steps determine a filetype.

" Line continuation is used here, remove 'C' from 'cpoptions'
let s:cpo_save = &cpoptions
set cpoptions&vim

" Put all autocmds in filetypedetect, as used by $VIMRUNTIME/filetype.vim
" to allow easy reset or removal.
augroup filetypedetect

" MSBuild imports
" https://docs.microsoft.com/en-us/visualstudio/msbuild/customize-your-build
au BufNewFile,BufRead *.props,*.targets
	\ if !did_filetype() && expand("<amatch>") !~ g:ft_ignore_pat
	\    && search('\C<Project', 'cnw')
	\|  setf xml
	\|endif

augroup END

" Restore 'cpoptions'
let &cpoptions = s:cpo_save
