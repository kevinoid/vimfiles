" File type detection for JSON with comments
" Note: Place in after/ftdetect to run after vim-json/ftdetect/json.vim
"       which calls setl ft=json for *.json on BufNewFile,BufRead

"
" Less certain entries.  setfiletype if not already set:
"

" https://github.com/mohae/cjson
autocmd BufNewFile,BufRead *.cjsn setfiletype jsonc

" https://github.com/mohae/cjson
autocmd BufNewFile,BufRead *.cjson setfiletype jsonc

" https://github.com/Microsoft/vscode/issues/48969
" https://komkom.github.io/
" https://github.com/mochajs/mocha/issues/3753
"autocmd BufNewFile,BufRead *.jsonc setfiletype jsonc

"
" More certain entries.  set filetype overriding any previous:
"

" https://eslint.org/docs/user-guide/configuring
autocmd BufNewFile,BufRead .eslintrc.json setlocal filetype=jsonc

" https://jshint.com/docs/
autocmd BufNewFile,BufRead .jshintrc setlocal filetype=jsonc

" https://mochajs.org/#configuring-mocha-nodejs
autocmd BufNewFile,BufRead .mocharc.json setlocal filetype=jsonc
autocmd BufNewFile,BufRead .mocharc.jsonc setlocal filetype=jsonc

" https://github.com/neoclide/coc.nvim
autocmd BufNewFile,BufRead coc-settings.json setlocal filetype=jsonc

" https://github.com/clutchski/coffeelint/pull/407
autocmd BufNewFile,BufRead coffeelint.json setlocal filetype=jsonc

" https://developer.mozilla.org/docs/Mozilla/Add-ons/WebExtensions/manifest.json
" TODO: Check for "manifest_version" to distinguish from other manifests
" which don't support comments (e.g. https://w3c.github.io/manifest/)
" FIXME: Only accepts // comments, not /* */
"autocmd BufNewFile,BufRead manifest.json setlocal filetype=jsonc

" https://github.com/microsoft/TypeScript/pull/5450
autocmd BufNewFile,BufRead tsconfig.json setlocal filetype=jsonc

" https://github.com/Alexays/Waybar/wiki/Configuration
autocmd BufNewFile,BufRead */waybar/config setlocal filetype=jsonc
