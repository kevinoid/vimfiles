" Generate a colorscheme with gui colors/attrs matching cterm colors/attrs.
" Maintainer:	Kevin Locke <kevin@kevinlocke.name>
" Last Change:	2022 Jan 07
" License:	CC0

" Converts a 4-bit (16-color) terminal color index to a color name
function highlightutils#CTermToName(ctermcolor) abort
    let colorind = v:null
    if type(a:ctermcolor) == v:t_number
        let colorind = a:ctermcolor
    elseif type(a:ctermcolor) == v:t_float
        \ && floor(a:ctermcolor) == a:ctermcolor
        let colorind = a:ctermcolor
    elseif type(a:ctermcolor) == v:t_string
        \ && a:ctermcolor =~# '^1[0-5]\|[0-9]$'
        let colorind = str2nr(a:ctermcolor)
    endif

    if colorind is# v:null || colorind < 0 || colorind > 15
        throw 'Invalid 4-bit color index: ' . a:ctermcolor
    endif

    if has('win32')
        let palette = [
            \ 'Black',
            \ 'DarkBlue',
            \ 'DarkGreen',
            \ 'DarkCyan',
            \ 'DarkRed',
            \ 'DarkMagenta',
            \ 'Brown',
            \ 'LightGray',
            \ 'DarkGray',
            \ 'Blue',
            \ 'Green',
            \ 'Cyan',
            \ 'Red',
            \ 'Magenta',
            \ 'Yellow',
            \ 'White'
        \ ]
    else
        let palette = [
            \ 'Black',
            \ 'DarkRed',
            \ 'DarkGreen',
            \ 'Brown',
            \ 'DarkBlue',
            \ 'DarkMagenta',
            \ 'DarkCyan',
            \ 'LightGray',
            \ 'DarkGray',
            \ 'Red',
            \ 'Green',
            \ 'Yellow',
            \ 'Blue',
            \ 'Magenta',
            \ 'Cyan',
            \ 'White'
        \ ]
    endif

    return palette[colorind]
endfunction

" Converts an 8-bit (256-color) terminal color index to #RRGGBB string
function highlightutils#CTermToGui(ctermcolor) abort
    let colorind = v:null
    if type(a:ctermcolor) == v:t_number
        let colorind = a:ctermcolor
    elseif type(a:ctermcolor) == v:t_float
        \ && floor(a:ctermcolor) == a:ctermcolor
        let colorind = a:ctermcolor
    elseif type(a:ctermcolor) == v:t_string
        \ && a:ctermcolor =~# '^[0-9]\{1,3\}$'
        let colorind = str2nr(a:ctermcolor)
    endif

    if colorind is# v:null || colorind < 0 || colorind > 255
        throw 'Invalid 8-bit color index: ' . a:ctermcolor
    endif

    " Math from https://github.com/GNOME/vte/blob/e32dba1d5cf884959af5b2c0691db00c263ea048/src/sixel-context.cc#L224
    if colorind < 16
        " 16-color palette
        if has('win32')
            " TODO: Support palettes from common apps?
            " https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit
            let palette = [
                \ '#000000',
                \ '#0000aa',
                \ '#00aa00',
                \ '#00aaaa',
                \ '#aa0000',
                \ '#aa00aa',
                \ '#aa5500',
                \ '#aaaaaa',
                \ '#555555',
                \ '#5555ff',
                \ '#55ff55',
                \ '#55ffff',
                \ '#ff5555',
                \ '#ff55ff',
                \ '#ffff55',
                \ '#ffffff',
            \ ]
        else
            let palette = [
                \ '#000000',
                \ '#aa0000',
                \ '#00aa00',
                \ '#aa5500',
                \ '#0000aa',
                \ '#aa00aa',
                \ '#00aaaa',
                \ '#aaaaaa',
                \ '#555555',
                \ '#ff5555',
                \ '#55ff55',
                \ '#ffff55',
                \ '#5555ff',
                \ '#ff55ff',
                \ '#55ffff',
                \ '#ffffff',
            \ ]
        endif
        let guifg = palette[colorind]
    elseif colorind < 232
        " Index into 6x6x6 color cube
        let colorind = colorind - 16
        let r = colorind / 36 * 40 + 55
        let g = colorind / 6 % 6 * 40 + 55
        let b = colorind % 6 * 40 + 55
        let guifg = printf(
            \ '#%02x%02x%02x',
            \ r == 55 ? 0 : r,
            \ g == 55 ? 0 : g,
            \ b == 55 ? 0 : b)
    else
        " Grayscale ramp
        let colorind = colorind - 232
        let g = colorind * 10 + 8
        let guifg = printf('#%02x%02x%02x', g, g, g)
    endif

    return guifg
endfunction

" Gets a string of highlight commands for the current highlight with gui
" attributes set to match cterm attributes.
function highlightutils#GenerateGuiColors() abort
    let ctcolors = execute('highlight')
    " Un-wrap any lines that wrapped
    let ctcolors = substitute(ctcolors, '\n \{19\}', ' ', 'g')
    let guilines = []
    for ctline in split(ctcolors, '\n')
        if stridx(ctline, ' links to ') >= 0
            \ || stridx(ctline, ' endswith') >= 0
            \ || stridx(ctline, ' cleared') >= 0
            continue
        endif

        let ctargvals = split(ctline, '\s\+')
        let guiargs = {}
        for ctargval in ctargvals[2:]
            let [ctarg, ctval] = split(ctargval, '=')
            let guiargs[ctarg] = ctval
        endfor

        if get(guiargs, 'cterm', 'NONE') isnot# 'NONE'
            let guiargs['gui'] = guiargs['cterm']
        elseif get(guiargs, 'gui', 'NONE') isnot# 'NONE'
            let guiargs['gui'] = 'NONE'
        endif

        if get(guiargs, 'ctermbg', 'NONE') isnot# 'NONE'
            let guiargs['guibg'] = highlightutils#CTermToGui(guiargs['ctermbg'])
        elseif get(guiargs, 'guibg', 'NONE') isnot# 'NONE'
            let guiargs['guibg'] = 'NONE'
        endif

        if get(guiargs, 'ctermfg', 'NONE') isnot# 'NONE'
            let guiargs['guifg'] = highlightutils#CTermToGui(guiargs['ctermfg'])
        elseif get(guiargs, 'guifg', 'NONE') isnot# 'NONE'
            let guiargs['guifg'] = 'NONE'
        endif

        " underline color is set by ctermul for cterm, guisp for gui
        " guisp is used for strikethrough/undercurl color on both
        " https://github.com/vim/vim/pull/6011
        " overwrite only for underline
        if get(guiargs, 'ctermul', 'NONE') isnot# 'NONE'
            \ && index(split(get(guiargs, 'cterm', ''), ','), 'underline') >= 0
            let guiargs['guifg'] = highlightutils#CTermToGui(guiargs['ctermul'])
        endif

        let guiargvals = []
        for [guiarg, guival] in items(guiargs)
            call add(guiargvals, guiarg . '=' . guival)
        endfor

        call add(guilines, 'highlight ' . ctargvals[0] . ' ' . join(guiargvals, ' '))
    endfor

    return join(guilines, "\n")
endfunction
