" Vim color scheme based on default cterm colors in VTE-based terminals
" Maintainer:	Kevin Locke <kevin@kevinlocke.name>
" Last Change:	2022 Jan 07
" License:	CC0
"
" Note: This colorscheme does not set background or highlight Normal to avoid
" clobbering terminal defaults or user preferences.
"
" Implementation Note: Created using :put =highlightutils#GenerateGuiColors()
" to convert the default color scheme for cterm to gui.

" Clear any previously defined highlight groups.
highlight clear

" Load the syntax highlighting defaults, if it's enabled.
if exists('syntax_on')
    syntax reset
endif

let g:colors_name = expand('<sfile>:t:r')

if &background is# 'light'
    highlight ColorColumn guibg=#ffd7d7
    highlight Comment guifg=#0000aa
    highlight Conceal guibg=#6c6c6c guifg=#aaaaaa
    highlight Constant guifg=#aa0000
    highlight CursorColumn guibg=#aaaaaa
    highlight CursorLine gui=underline guibg=NONE
    highlight CursorLineNr gui=underline guifg=#af5f00
    highlight DiagnosticError guifg=#aa0000
    highlight DiagnosticHint guifg=#aaaaaa
    highlight DiagnosticInfo guifg=#0000aa
    highlight DiagnosticUnderlineError gui=underline guisp=Red
    highlight DiagnosticUnderlineHint gui=underline guisp=LightGrey
    highlight DiagnosticUnderlineInfo gui=underline guisp=LightBlue
    highlight DiagnosticUnderlineWarn gui=underline guisp=Orange
    highlight DiagnosticWarn guifg=#aa5500
    highlight DiffAdd guibg=#5fd7ff
    highlight DiffChange guibg=#ffd7ff
    highlight DiffDelete gui=NONE guibg=#afffff guifg=#5555ff
    highlight Directory guifg=#0000aa
    highlight ErrorMsg guibg=#aa0000 guifg=#ffffff
    highlight FoldColumn guibg=#a8a8a8 guifg=#0000aa
    highlight Folded guibg=#a8a8a8 guifg=#0000aa
    highlight Identifier guifg=#00aaaa
    highlight Ignore guifg=#ffffff
    highlight IncSearch gui=reverse
    highlight LineNr guifg=#af5f00
    highlight MatchParen guibg=#55ffff
    highlight MoreMsg gui=NONE guifg=#00aa00
    highlight NonText gui=NONE guifg=#5555ff
    highlight Pmenu guibg=#ffd7ff guifg=#000000
    highlight PmenuSbar guibg=#a8a8a8
    highlight PmenuSel guibg=#aaaaaa guifg=#000000
    highlight PmenuThumb guibg=#000000
    highlight PreProc guifg=#aa00aa
    highlight Question gui=NONE guifg=#00aa00
    highlight RedrawDebugClear guibg=#ffff55
    highlight RedrawDebugComposed guibg=#55ff55
    highlight RedrawDebugNormal gui=reverse
    highlight RedrawDebugRecompose guibg=#ff5555
    highlight Search guibg=#ffff55
    highlight SignColumn guibg=#a8a8a8 guifg=#0000aa
    highlight Special guifg=#aa00aa
    highlight SpecialKey guifg=#0000aa
    highlight Statement gui=NONE guifg=#af5f00
    highlight StatusLineTerm gui=bold guibg=#00aa00 guifg=#ffffff
    highlight StatusLineTermNC guibg=#00aa00 guifg=#ffffff
    highlight TabLine gui=underline guibg=#aaaaaa guifg=#000000
    highlight Title gui=NONE guifg=#aa00aa
    highlight Todo guibg=#ffff55 guifg=#000000
    highlight ToolbarButton gui=bold guibg=#6c6c6c guifg=#ffffff
    highlight ToolbarLine guibg=#aaaaaa
    highlight Type gui=NONE guifg=#00aa00
    highlight Underlined gui=underline guifg=#aa00aa
    highlight Visual guibg=#aaaaaa
    highlight WarningMsg guifg=#aa0000
    highlight WildMenu guibg=#ffff55 guifg=#000000
else
    highlight ColorColumn guibg=#aa0000
    highlight Comment guifg=#55ffff
    highlight Conceal guibg=#6c6c6c guifg=#aaaaaa
    highlight Constant guifg=#ff55ff
    highlight CursorColumn guibg=#6c6c6c
    highlight CursorLine gui=underline guibg=NONE
    highlight CursorLineNr gui=underline guifg=#ffff55
    highlight DiagnosticError guifg=#aa0000
    highlight DiagnosticHint guifg=#aaaaaa
    highlight DiagnosticInfo guifg=#0000aa
    highlight DiagnosticWarn guifg=#aa5500
    highlight DiffAdd guibg=#0000aa
    highlight DiffChange guibg=#aa00aa
    highlight DiffDelete gui=NONE guibg=#00aaaa guifg=#5555ff
    highlight Directory guifg=#afffff
    highlight ErrorMsg guibg=#aa0000 guifg=#ffffff
    highlight FoldColumn guibg=#6c6c6c guifg=#55ffff
    highlight Folded guibg=#6c6c6c guifg=#55ffff
    highlight Identifier gui=bold guifg=#55ffff
    highlight Ignore guifg=#000000
    highlight IncSearch gui=reverse
    highlight LineNr guifg=#ffff55
    highlight MatchParen guibg=#00aaaa
    highlight ModeMsg gui=bold
    highlight MoreMsg gui=NONE guifg=#87ffaf
    highlight NonText gui=NONE guifg=#5555ff
    highlight Pmenu guibg=#ff55ff guifg=#000000
    highlight PmenuSbar guibg=#a8a8a8
    highlight PmenuSel guibg=#000000 guifg=#6c6c6c
    highlight PmenuThumb guibg=#ffffff
    highlight PreProc guifg=#5fd7ff
    highlight Question gui=NONE guifg=#87ffaf
    highlight RedrawDebugClear guibg=#ffff55
    highlight RedrawDebugComposed guibg=#55ff55
    highlight RedrawDebugRecompose guibg=#ff5555
    highlight Search guibg=#ffff55 guifg=#000000
    highlight SignColumn guibg=#6c6c6c guifg=#55ffff
    highlight Special guifg=#ffd7d7
    highlight SpecialKey guifg=#5fd7ff
    highlight Statement gui=NONE guifg=#ffff55
    highlight StatusLineTerm gui=bold guibg=#87ffaf guifg=#000000
    highlight StatusLineTermNC guibg=#87ffaf guifg=#000000
    highlight TabLine gui=underline guibg=#6c6c6c guifg=#ffffff
    highlight Title gui=NONE guifg=#ffd7ff
    highlight Todo guibg=#ffff55 guifg=#000000
    highlight ToolbarButton gui=bold guibg=#aaaaaa guifg=#000000
    highlight ToolbarLine guibg=#6c6c6c
    highlight Type gui=NONE guifg=#87ffaf
    highlight Underlined gui=underline guifg=#5fd7ff
    highlight Visual guibg=#6c6c6c
    if &t_Co >= 256
        " Default is ctermfg=224 guifg=Red
        " I think 224 is too white, use 214 instead
        highlight WarningMsg ctermfg=214
    endif
    highlight WarningMsg guifg=#ffaf00
    highlight WildMenu guibg=#ffff55 guifg=#000000
endif

" ctermbg=Red is too bright on my eyes.
" Change ctermbg=Red to ctermbg=DarkRed.  Use intermediate guibg.
highlight DiffText gui=bold ctermbg=DarkRed term=reverse guibg=#dd3333 cterm=bold
highlight Error ctermbg=DarkRed ctermfg=White term=reverse guibg=#dd3333 guifg=#ffffff

" Use underline/undercurl for Spell*
highlight SpellBad   ctermbg=NONE cterm=undercurl term=undercurl gui=undercurl guisp=Red
highlight SpellCap   ctermbg=NONE cterm=undercurl term=undercurl gui=undercurl guisp=Blue
highlight SpellRare  ctermbg=NONE cterm=undercurl term=undercurl gui=undercurl guisp=Magenta
highlight SpellLocal ctermbg=NONE cterm=undercurl term=undercurl gui=undercurl guisp=Cyan
" Use colored underline if supported <https://github.com/vim/vim/pull/6011>
if has('patch-8.2.0863')
    highlight SpellBad   ctermul=Red
    highlight SpellCap   ctermul=Blue
    highlight SpellRare  ctermul=Magenta
    highlight SpellLocal ctermul=Cyan
endif
