" Early file type detection
"
" Maintainer:	Kevin Locke <kevin@kevinlocke.name>
" Last Change:	2021 July 16
"
" File type detection is split into 4 phases (see :help 43.2):
" 1. filetype.vim precise path pattern matching.
" 2. scripts.vim content matching (typically from first few lines).
" 3. filetype.vim loose path pattern matching.
" 4. ftdetect/*.vim from plugins.
"
" This script contains precise path patterns for early matching.

" Put all autocmds in filetypedetect, as used by $VIMRUNTIME/filetype.vim
" to allow easy reset or removal.
augroup filetypedetect

"
" Match by filename (in alphabetical order, ignoring leading .)
"

" GRUB 2 config file
" Although the syntax is not identical, the GRUB Manual describes "syntax quite
" similar to that of GNU Bash and other Bourne shell derivatives".
" https://www.gnu.org/software/grub/manual/grub/grub.html#Shell_002dlike-scripting
au BufNewFile,BufRead grub.cfg			setf sh

" Rclone config file
" https://rclone.org/docs/#config-config-file
" TODO: Handle encrypted config file (NaCl SecretBox)?
" https://rclone.org/docs/#configuration-encryption
au BufNewFile,BufRead rclone.conf		setf dosini
au BufNewFile,BufRead .rclone.conf		setf dosini


"
" Match by file extensions (in alphabetical order)
"

" Atom Syndication Format (RFC 4287)
" https://github.com/vim/vim/pull/7986
if !has('patch-8.2.2624')
    au BufNewFile,BufRead *.atom		setf xml
endif

" Mozilla JavaScript Modules
au BufNewFile,BufRead *.jsm			setf javascript

" JSON Patch (RFC 6902)
" https://github.com/vim/vim/pull/8450
if !has('patch-8.2.3049')
    au BufNewFile,BufRead *.json-patch		setf json
endif

" Markdown (not modula2)
" This was fixed in commit 7d76c804a first included in 7.4.480
if !has('patch-7.4.480')
    au BufNewFile,BufRead *.md			setf markdown
endif

" PHP Test
" TODO: Write ftplugin for phpt
" https://www.phpinternalsbook.com/tests/phpt_file_structure.html
au BufNewFile,BufRead *.phpt			setf php

" MSBuild Response Files
" https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-response-files
au BufNewFile,BufRead *.rsp			setf conf

" RSS (RDF Site Summary/Really Simple Syndication)
" https://github.com/vim/vim/pull/7987
if !has('patch-8.2.2625')
    au BufNewFile,BufRead *.rss			setf xml
endif

" Visual Studio Settings File
au BufNewFile,BufRead *.vssettings		setf xml

" Web Services Description Language
" https://www.w3.org/TR/wsdl
" https://github.com/vim/vim/pull/3281
if !has('patch-8.1.0273')
    au BufNewFile,BufRead *.wsdl		setf xml
endif


"
" Match by path (in alphabetical order by filetype)
"

" NetworkManager config file
" FIXME: Create ftplugin for specific NetworkManager.conf quirks
au BufNewFile,BufRead *.nmconnection						setf dosini
au BufNewFile,BufRead /etc/NetworkManager/NetworkManager.conf			setf dosini
au BufNewFile,BufRead /etc/NetworkManager/conf.d/*.conf				setf dosini
au BufNewFile,BufRead /run/NetworkManager/conf.d/*.conf				setf dosini
au BufNewFile,BufRead /usr/lib/NetworkManager/conf.d/*.conf			setf dosini
au BufNewFile,BufRead /var/lib/NetworkManager/NetworkManager-intern.conf	setf dosini

" iptables-save(8) rule files in iptables-persistent directory
au BufNewFile,BufRead /etc/iptables/*				setf iptables

" Jekyll drafts and posts
" Use Liquid rather than Markdown so that YAML frontmatter and liquid tags are
" highlighted correctly
au BufNewFile,BufRead */_drafts/*.markdown			setf liquid
au BufNewFile,BufRead */_posts/*.markdown			setf liquid

" Systemd overrides and temp files
" https://github.com/vim/vim/pull/5914
if !has('patch-8.1.1048')
    " Systemd overrides
    au BufNewFile,BufRead /etc/systemd/system/*.d/*.conf	setf systemd
    " Systemd temp files
    au BufNewFile,BufRead /etc/systemd/system/*.d/.#*		setf systemd
endif

augroup END
