" File type detection for OpenVPN configuration files
autocmd BufNewFile,BufRead /etc/openvpn/*.conf      setfiletype openvpn
autocmd BufNewFile,BufRead *.ovpn                   setfiletype openvpn
