" Local preferences file for Vim

" Start with system defaults, if they exist (see :help defaults.vim in Vim 8+)
if !empty(glob($VIMRUNTIME.'/defaults.vim'))
    source $VIMRUNTIME/defaults.vim
endif

if has('win32')
    " Force UTF-8 on Windows, since $LANG and terminal detection is unreliable
    " UTF-8 works better than latin1 in cmd.exe and gVim for me
    " See https://unix.stackexchange.com/a/23414
    set encoding=utf-8
endif

set nofixendofline     " Don't add <EOL> at end of files lacking them
                       " It adds too much churn in version control
set showcmd            " Show (partial) command in status line.
set showmatch          " Show matching brackets.
set hlsearch           " Highlight search results
set incsearch          " Incremental search
set history=1000       " Remember what I've done for longer
set mouse=             " Disable mouse, which I don't often use
set undofile           " Save undo information for files (:undo-persistence)
set visualbell	       " Use terminal visual bell in place of beep

" quickfix shortcuts
" https://begriffs.com/posts/2019-07-19-history-use-vim.html#edit-compile-cycle
nmap ]q :lnext<cr>
nmap ]Q :llast<cr>
nmap [q :lprev<cr>
nmap [Q :lfirst<cr>

" Path to directory where local state should be stored.
" See https://specifications.freedesktop.org/basedir-spec/
let s:statedir=$XDG_STATE_HOME
if s:statedir is# '' && $HOME isnot# ''
    if has('win32')
        let s:statedir=$HOME . '/AppData/Local'
    else
        let s:statedir=$HOME . '/.local/state'
    endif
endif
if s:statedir isnot# ''
    if has('nvim')
        let s:statedir=s:statedir . '/nvim'
    else
        let s:statedir=s:statedir . '/vim'
    endif

    " Save backup, swap, and undo files in statedir
    " Saving alongside the edited file allows sharing between users, but has
    " more significant disadvantages for my most common uses, where it causes
    " problems with build systems that rebuild on file changes (e.g. XSDs in
    " ASP.NET Web Site Projects) and lots of unnecessary clutter on unclean
    " exit.
    let &backupdir=s:statedir . '/backup/'
    call mkdir(&backupdir, 'p')
    if exists('&shadafile')
        let &shadafile=s:statedir . '/shada/main.shada'
        call mkdir(s:statedir . '/shada', 'p')
    endif
    " Note: Ends with '//' to build swap path from file path
    let &directory=s:statedir . '/swap//'
    call mkdir(&directory, 'p')
    " Note: Ends with '//' to build undo path from file path
    let &undodir=s:statedir . '/undo//'
    call mkdir(&undodir, 'p')
endif

" Look for ctags above current directory
" https://stackoverflow.com/a/741486
set tags=./tags,./TAGS,tags,TAGS;/

" Use ripgrep or silver searcher for grep when available
if executable('rg')
    set grepprg=rg\ -H\ --column\ $*
    set grepformat=%f:%l:%c:%m
elseif executable('ag')
    " Note: Add /dev/null argument to get filenames in output.
    " https://github.com/ggreer/the_silver_searcher/issues/972
    set grepprg=ag\ --column\ $*\ /dev/null
    set grepformat=%f:%l:%c:%m
end

" Turn on indenting
set softtabstop=4 shiftwidth=4

" Treat /bin/sh as POSIX shell rather than legacy sh
let g:is_posix=1

" Enable JSDoc highlighting in JavaScript
let g:javascript_plugin_jsdoc = 1

" Disable quote concealing in vim-json, which I find confusing
let g:vim_json_syntax_conceal = 0

" Disable folding provided by vim-markdown, which has serious issues:
" https://github.com/plasticboy/vim-markdown/issues/427
" https://github.com/plasticboy/vim-markdown/issues/455
let g:vim_markdown_folding_disabled = 1

" Use 2-space indent for Markdown lists
" https://github.com/plasticboy/vim-markdown/#adjust-new-list-item-indent
let g:vim_markdown_new_list_item_indent = 2

" Use xmledit ftplugin when editing HTML
let g:xml_use_xhtml = 1
let g:xmledit_enable_html = 1

" Enable language highlighting for code blocks in Markdown
" Note:  Applying all syntaxes caused massive errors, not sure why...
"let g:markdown_fenced_languages2 = map(globpath(&rtp, 'syntax/*.vim', 0, 1), 'substitute(v:val, ''.*/\(.*\)\.vim$'', ''\1'', '''')')
" Enable languages that we are likely to use and alias for pygments names.
let g:markdown_fenced_languages = [
   \ 'apache',
   \ 'c',
   \ 'cpp',
   \ 'css',
   \ 'html',
   \ 'java',
   \ 'javascript',
   \ 'js=javascript',
   \ 'json',
   \ 'ruby',
   \ 'sass',
   \ 'scala',
   \ 'sh',
   \ 'sql',
   \ 'xml',
   \ 'xhtml',
   \ 'yaml'
   \ ]

" Add ALE to statusline, simulating default around it similar to Syntastic
" https://unix.stackexchange.com/a/243667
function! LinterStatus() abort
    let counts = ale#statusline#Count(bufnr(''))
    let num_errors = counts.error + counts.style_error
    let num_warnings = counts.warning + counts.style_warning
    return '[Lint: ' .
        \ (counts.total == 0 ? 'OK' :
            \ printf('%dE %dW', num_errors, num_warnings)) .
        \ ']'
endfunction
set statusline=%f\ %h%w%m%r\ 
set statusline+=%#warningmsg#
set statusline+=%{LinterStatus()}
set statusline+=%*
set statusline+=%=%(%l,%c%V\ %=\ %P%)

" EditorConfig configuration
" Don't apply EditorConfig to files under .git (especially COMMIT_EDITMSG)
let g:EditorConfig_exclude_patterns = ['/\.git/']

" ALE configuration
" Add linter name to messages
let g:ale_echo_msg_format = '%code%: %s [%linter%]'
" Set fixers for some filetypes
let g:ale_fixers = {
\   'go': ['gofmt'],
\   'javascript': ['eslint'],
\   'json': ['jq'],
\   'python': ['isort', 'black'],
\   'sh': ['shfmt'],
\   'xml': ['xmllint'],
\}
" Check for unused disable directives when running ESLint
let g:ale_javascript_eslint_options = '--report-unused-disable-directives'
" Don't lint on BufWinEnter or FileType event (makes :argdo painfully slow)
" See https://github.com/dense-analysis/ale/issues/1338
" Disabled because lint on open is desirable.  Use :ALEDisable as required.
" TODO: Consider linting after a delay for these events and skip if closed.
"let g:ale_lint_on_enter = 0
"let g:ale_lint_on_filetype_changed = 0
" Don't lint files in response to text changes (annoying and excessive)
let g:ale_lint_on_text_changed = 'never'
" Add missing linter aliases
" ps1: See https://github.com/dense-analysis/ale/pull/3010
let g:ale_linter_aliases = {
\   'ps1': 'powershell',
\}
" Set linters for some filetypes
" sh: shell linter fails on scripts where shopt changes parsing (e.g. extglob).
"     Could convert shopt to -O options in some cases, but would be fragile.
"     shellcheck is sufficient for now.
let g:ale_linters = {
\   'sh': ['shellcheck']
\}
" Show 5 lines of errors (default: 10)
let g:ale_list_window_size = 5
" Don't lint minified files
let g:ale_pattern_options = {
    \ '\.min\.css$': {'ale_linters': [], 'ale_fixers': []},
    \ '\.min\.js$': {'ale_linters': [], 'ale_fixers': []},
    \ }
" Use binaries inside pipenv, if present
let g:ale_python_auto_pipenv = 1
" By default, ALE runs vulture on the project root which often includes files
" which should not be vulture'd (virtualenvs, docs, tests).  Exclude.
let g:ale_python_vulture_options = '--exclude ' . join([
    \ '*/site-packages/*',
    \ '*/.env/*',
    \ '*/.env[23]/*',
    \ '*/.tox/*',
    \ '*/.venv/*',
    \ '*/docs/*',
    \ '*/env/*',
    \ '*/env[23]/*',
    \ '*/tests/*',
    \ '*/venv/*',
    \ '*/venv[23]/*',
    \ ], ',')
" Don't pass -s to shellcheck, since it overrides shell directives
let g:ale_sh_shellcheck_dialect = ''

" Turn on spell-checking by default
set spell spelllang=en_us

" Enable filetype indenting and plugins
filetype indent on
filetype plugin on

" Change cursor shape in different modes
" https://vimhelp.org/term.txt.html#termcap-cursor-shape
" https://vim.fandom.com/wiki/Change_cursor_shape_in_different_modes
" FIXME: Any risk the escape sequences could be misinterpreted?  If so, could
" limit to has_key(term_getcursor(bufnr())[2], 'shape') to check t_RC response
" (since v:termstyleresp is not set before it is called, apparently)
" TODO: Use same logic as neovim:
" https://github.com/neovim/neovim/blob/09d270bcea5f81a0772e387244cc841e280a5339/src/nvim/tui/tui.c#L1810-L1898
"
" Note: Disabled for win32 before 9.0.0347, since conhost supports the Windows
" Cursor API and all VT sequences I tried on Windows Terminal caused issues:
" https://github.com/microsoft/terminal/issues/68#issuecomment-596166837
" https://github.com/microsoft/terminal/issues/4335
" https://github.com/microsoft/terminal/issues/7233#issuecomment-677987393
" https://github.com/vim/vim/issues/6576
" https://github.com/vim/vim/pull/5546
" Note: Works fine in msys/cygwin vim (e.g. with git) on Windows Terminal.
"
" Note: neovim ignores these: https://neovim.io/doc/user/vim_diff.html#t_SI
if has('cursorshape') && (!has('win32') || has('patch-9.0.347'))
    let &t_SI = "\<Esc>[6 q"
    let &t_SR = "\<Esc>[4 q"
    let &t_EI = "\<Esc>[2 q"
endif

if has('gui_running')
    " Note: The default background color is set by system:
    " - GetSysColor(COLOR_WINDOW) on Windows
    " - -bg/-background CLI option for GTK
    " - XtDefaultBackground on X11 (Athena or Motif)
    " However, I rarely want Vim to match the system default window color.
    " I prefer Vim to be dark regardless of system colors.
    set background=dark

    " Vim 9.1.0915 changed the default GTK font to Monospace 12
    " Revert it.
    if has('patch-9.1.915')
        if has('win32')
            set guifont=Consolas:h10
        elseif has('gui_gtk')
            set guifont=Monospace\ 10
        endif
    endif

" &background is detected in Neovim 0.4.0 and later, Vim 7.0 and later:
" https://github.com/neovim/neovim/pull/9509
" https://github.com/vim/vim/commit/071d4279d6#diff-31a4348fe4d148d46829112ab401400c4ed23a2ce0df872fc150f0c40d647b6c
" On Vim it's only reliable for &term == builtin_gui or $COLORFGBG:
" https://github.com/vim/vim/issues/869
" https://github.com/vim/vim/blob/v8.2.4035/src/term.c#L3013-L3045
elseif !has('nvim-0.4.0')
    \ && (v:version < 700 || has('nvim')
        \ || (!has('gui_running') && $COLORFGBG is# ''))
    " Not detected.  Assume dark.
    set background=dark
endif
syntax on
colorscheme interminable
" interminable color scheme doesn't set Normal (to not clobber term defaults).
" Set for gvim where I prefer dark and don't always change system colors.
if has('gui_running')
    highlight Normal guifg=LightGrey guibg=Black
endif

" Try to enable termguicolors on Windows virtual terminal, where t_Co=16 by
" default <https://github.com/vim/vim/issues/9498> and `set t_Co=256` screws
" up colors badly. See <https://github.com/vim/vim/issues/9836>
"
" Note: Incompatible with transparent background before patch-8.2.3516:
" https://github.com/vim/vim/issues/2361
" May still be incompatible with transparent background on Windows Terminal:
" https://github.com/vim/vim/issues/7162
"
" Note: Could enable termguicolors for more colors on more terminals by
" checking $COLORTERM =~# '^truecolor|24bit$' (or RGB in terminfo?) for support
" https://github.com/termstandard/colors#truecolor-detection
" interminable color scheme doesn't currently use any.  Leave disabled for now.
"
" FIXME: Temporarily disabled due to termguicolors incompatibility with pipes
" from msys (e.g. git): https://github.com/microsoft/terminal/issues/12289
if v:false && has('win32') && has('vcon')
    try
        set termguicolors
    catch /^Vim(set):E954:/
        " Does not work when "Legacy Console Mode" is enabled:
        " https://docs.microsoft.com/windows/console/legacymode
        " May not work on Windows < 10.0.10240:
        " https://github.com/vim/vim/pull/5546#issuecomment-603443412
        " Could check system('wmic os get Version /value') if necessary.
        " has('vcon') check should detect both of these.
    endtry
endif

" Clear $MANOPT so no options which confuse man.vim get passed through
let $MANOPT=''
" Load the man plugin so that we can run :Man
" Note: Moved to plugin/man.vim (loaded automatically) in neovim
if !has('nvim')
    runtime ftplugin/man.vim
endif

if !has('autocmd')
    finish
endif

augroup fileTypeIndent
    autocmd!

    " Note: b:EditorConfig_applied is true when settings from .editorconfig
    " have been applied to the buffer.
    " Defined by after/plugin/editorconfig_applied.vim

    " Set bzr commit line length to match git convention of 72
    autocmd FileType bzr
        \ if !get(b:, 'EditorConfig_applied') | setlocal tw=72 | endif
    autocmd FileType cmake
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=2 sw=2 et | endif
    autocmd FileType css
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=8 sw=8 ts=8 noet | endif
    autocmd FileType {less,sass,scss}
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=2 sw=2 et | endif
    autocmd FileType gitconfig
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=8 sw=8 | endif
    autocmd FileType {html,xhtml,xml,xslt}
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=2 sw=2 et | endif
    autocmd FileType {php,c,cpp,java}
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=4 sw=4 et | endif
    autocmd FileType lua
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=8 sw=8 | endif
    autocmd FileType markdown
        \ if !get(b:, 'EditorConfig_applied') |  setlocal et tw=78 | endif
    autocmd FileType make
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=8 sw=8 noet | endif
    autocmd FileType mail
        \ if !get(b:, 'EditorConfig_applied') |  setlocal tw=70 | endif
    autocmd FileType php
        \ if !get(b:, 'EditorConfig_applied') |  setlocal noet ts=4 | endif
    " Note: PS1 indentation size follows PowerShell ISE convention.
    " https://poshcode.gitbooks.io/powershell-practice-and-style/Style-Guide/Code-Layout-and-Formatting.html#indentation
    autocmd FileType ps1
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=4 sw=4 et | endif
    autocmd FileType python
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=4 sw=4 et | endif
    autocmd FileType rst
        \ if !get(b:, 'EditorConfig_applied') |  setlocal et tw=80 | endif
    autocmd FileType {json,javascript,ruby}
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=2 sw=2 et | endif
    autocmd FileType scala
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=2 sw=2 et tw=80 | endif
    autocmd FileType sh
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=8 sw=8 noet | endif
    autocmd FileType vim
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=4 sw=4 et | endif
    autocmd FileType yaml
        \ if !get(b:, 'EditorConfig_applied') |  setlocal sts=2 sw=2 et | endif
augroup END

augroup fileTypeSettings
    autocmd!

    " Disable XML reindenting on keystrokes other than CTRL-F and new lines
    autocmd FileType {ant,docbk,xml,xsd,xslt} setlocal indentkeys=!^F,o,O
    " Set keyword characters appropriately for XML
    autocmd FileType {ant,docbk,xml,xsd,xslt} setlocal iskeyword=@,-,\:,48-57,_,128-167,224-235
    " Disable HTML reindenting on keystrokes other than CTRL-F and new lines
    autocmd FileType {eruby,html,htmldjango,jsp,liquid,xhtml} setlocal indentkeys=!^F,o,O
    autocmd FileType php setlocal indentexpr= cindent
    " Case is rarely significant in PowerShell.  Ignore by default.
    autocmd FileType ps1 setlocal ignorecase
    autocmd FileType scala setlocal fo=croql
    " Disable YAML reindenting on keystrokes other than CTRL-F and new lines
    autocmd FileType yaml setlocal indentkeys=!^F,o,O
augroup END

" Update vimStartup autocmd group to exclude commits
" https://github.com/vim/vim/commit/9a48961d8bd7ffea14330b9b0181a6cdbe9288f7
augroup vimStartup
    autocmd!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif
augroup END
